library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity dac_top is
  port ( mclk : in std_logic;
         rst : in std_logic;
         dat_valid : in std_logic;
      -- Interfacing signal to DAC084S045
         dac_sclk : out std_logic;
         dac_sync_n : out std_logic;
         dac_sout : out std_logic;
        --
        in1 : in std_logic_vector(7 downto 0);
        in2 : in std_logic_vector(7 downto 0);
        in3 : in std_logic_vector(7 downto 0);
        in4 : in std_logic_vector(7 downto 0)
        );
end entity dac_top;

architecture str of dac_top is

component dac084s085_interface
  port (-- Global Clock and Reset
         mclk : in std_logic;
         mrst : in std_logic;
        -- Interface signals to module interacting with DAC
            dac_data : in  std_logic_vector(7 downto 0);
              dac_ch : in  std_logic_vector(1 downto 0);
             dac_cmd : in  std_logic_vector(1 downto 0);
              dac_wr : in  std_logic;
         dac_wr_done : out std_logic;
        -- Interfacing signal to DAC084S085
           sclk : out std_logic;
         sync_n : out std_logic;
           sout : out std_logic
       );
end component;

signal dac_data : std_logic_vector(7 downto 0) := (others => '1');
signal dac_channel : std_logic_vector(1 downto 0) := "00";

begin

inst_dac084s085_interface : dac084s085_interface port map (
         mclk => mclk,
         mrst => rst,
     dac_data => dac_data,
       dac_ch => dac_channel,
      dac_cmd => "01",
       dac_wr => dat_valid,
  dac_wr_done => open,
         sclk => dac_sclk,
       sync_n => dac_sync_n,
         sout => dac_sout
  );

  dac_read_proc: process (mclk) begin
  if rising_edge(mclk) then
    if (dat_valid = '1') then

            if(dac_channel = "00") then
               dac_data <=in1;
              dac_channel <= "01"  ;
              elsif (dac_channel = "01") then
             dac_data<= in2;
              dac_channel <= "10";
             elsif (dac_channel = "10") then
               dac_data <= in3;
             dac_channel <= "11" ;
             elsif (dac_channel = "11") then
               dac_data <= in4;
             dac_channel <= "00" ;
             end if;
     end if;
     end if;

  end process;

  end architecture str;
