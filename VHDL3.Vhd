
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all ;

entity mux2 is
    Port (
           clk : in std_logic;
           data_in2 : in std_logic_vector(7 downto 0);
           D_ia : out std_logic_vector(7 downto 0);
           D_ib : out std_logic_vector(7 downto 0);
           D_ic : out std_logic_vector(7 downto 0);
           D_v : out std_logic_vector(7 downto 0);


           cnt2 : out std_logic_vector(2 downto 0)
           );
end mux2;

architecture Behavioral of mux2 is
signal cnt : std_logic_vector(2 downto 0):= "000";
signal data : std_logic_vector(7 downto 0) := (others => '0');
constant const : std_logic_vector(3 downto 0) := "0000";

begin
process(clk)

begin
 if rising_edge(clk) then
 if( cnt = "000") then
           D_ic<= data_in2;
           cnt<= "001";
        elsif( cnt = "001") then
           D_v<= data_in2;
           cnt<= "010";
        elsif( cnt = "010") then
           D_ia<= data_in2;
           cnt<= "011";
        elsif( cnt = "011") then
           D_ib<= data_in2;
           cnt<= "000";


         end if;
         cnt2 <= cnt;
end if;
end process;

end Behavioral;

