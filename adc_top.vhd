library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 

entity adc_top is
  port ( mclk : in std_logic;
         rst : in std_logic;
      -- Interfacing signal to ADC084S085
         adc_sclk : out std_logic;
         adc_cs_n : out std_logic;
         adc_sout : out std_logic;
          adc_sin : in  std_logic;
         data_valid : out  std_logic;

        --
        ch0 : out std_logic_vector(7 downto 0);
        ch1 : out std_logic_vector(7 downto 0);
        ch2 : out std_logic_vector(7 downto 0)
        );
end entity adc_top;

architecture str of adc_top is

component dac084s085_interface
  port (-- Global Clock and Reset
         mclk : in std_logic;
         mrst : in std_logic;
        -- Interface signals to module interacting with DAC
            dac_data : in  std_logic_vector(7 downto 0);
              dac_ch : in  std_logic_vector(1 downto 0);
             dac_cmd : in  std_logic_vector(1 downto 0);
              dac_wr : in  std_logic;
         dac_wr_done : out std_logic;
        -- Interfacing signal to DAC084S085
           sclk : out std_logic;
         sync_n : out std_logic;
           sout : out std_logic
       );
end component;

component adc084s021_interface
  port (-- Global Clock and Reset
         mclk : in std_logic;
         mrst : in std_logic;
        -- Interface signals to module interacting with ADC
               adc_data : out std_logic_vector(7 downto 0);
         adc_data_valid : out std_logic;
                 adc_ch : in  std_logic_vector(1 downto 0);
              adc_rd_en : in  std_logic;
        -- Interfacing signal to ADC084S085
         sclk : out std_logic;
         cs_n : out std_logic;
         sout : out std_logic;
          sin : in  std_logic
       );
end component;

signal adc_data : std_logic_vector(7 downto 0) := (others => '0');
signal adc_data_ch0 : std_logic_vector(7 downto 0) := (others => '0');
signal adc_data_ch1 : std_logic_vector(7 downto 0) := (others => '0');
signal adc_data_ch2 : std_logic_vector(7 downto 0) := (others => '0');
signal adc_data_valid : std_logic := '0';
signal adc_rd_en : std_logic;
signal adc_channel : std_logic_vector(1 downto 0) := "00";


begin


inst_adc084s021_interface : adc084s021_interface port map (
            mclk => mclk,
            mrst => rst,
        adc_data => adc_data,
  adc_data_valid => adc_data_valid,
          adc_ch => adc_channel,
       adc_rd_en => adc_rd_en,
             sin => adc_sin,
            sclk => adc_sclk,
            cs_n => adc_cs_n,
            sout => adc_sout
  );



adc_read_proc: process (mclk) begin
  if rising_edge(mclk) then
    if (rst = '1') then
      adc_rd_en <= '1';
    else
      adc_rd_en <= adc_data_valid;
    end if;

    if (adc_data_valid = '1') then
            if (adc_channel = "00") then
            adc_data_ch0 <=  adc_data;
            adc_channel <= "01";
             elsif (adc_channel = "01") then
             adc_data_ch1 <=  adc_data;
              adc_channel <= "10";
             elsif (adc_channel = "10") then
              adc_data_ch2 <=  adc_data;
             adc_channel <= "00" ;
              end if;

              end if;



    --



data_valid<= adc_data_valid;
end if;


end process adc_read_proc;
   ch0 <= adc_data_ch1;
    ch1 <= adc_data_ch2;
    ch2 <= adc_data_ch0;

end architecture str;
