
library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------
entity adc084s021_interface is
  port (-- Global Clock and Reset
         mclk : in std_logic;
         mrst : in std_logic;
        -- Interface signals to module interacting with ADC
               adc_data : out std_logic_vector(7 downto 0);
         adc_data_valid : out std_logic;
                 adc_ch : in  std_logic_vector(1 downto 0);
              adc_rd_en : in  std_logic;
        -- Interfacing signal to ADC084S085
         sclk : out std_logic;
         cs_n : out std_logic;
         sout : out std_logic;
          sin : in  std_logic
       );
end entity adc084s021_interface;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
architecture rtl of adc084s021_interface is

--------------------------------------------------------------------------------
--
-- Format of Data Input to ADC
--   __   __   __   __   __   __   __   __
--  |07| |06| |05| |04| |03| |02| |01| |00|
--
--  7 to 6 and 2 to 0 => Dont Care
--  5 => ADD2 -> Dont Care
--  4 & 3 -> selects input channel
--  00 -> IN1 (default)
--  01 -> IN2
--  10 -> IN3
--  11 -> IN4
--
--  Next 8 bits Dont Care
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--
-- Format of Data Output of ADC
--   __   __   __   __   __   __   __   __   __   __   __   __   __   __   __   __
--  |15| |14| |13| |12| |11| |10| |09| |08| |07| |06| |05| |04| |03| |02| |01| |00|
--
--  15 to 12 => High Impedence - Dont Care
--
--  11 to 4 => Data output of ADC. MSB First
--
--  3 to 0 => Zeros - Dont Care
--
--------------------------------------------------------------------------------

-- ADC MAX Clock Rate is 3.2 MHz. Here it is set to 2.5 MHz
signal clk_en : std_logic_vector(2 downto 0) := "001";
signal sclk_int : std_logic := '1';
signal cs_n_int : std_logic := '1';
signal cs_n_int_sr : std_logic_vector(4 downto 0) := (others => '1');

signal sout_sr : std_logic_vector( 5 downto 0) := (others => '0');
signal  sin_sr : std_logic_vector(15 downto 0) := (others => '0');

signal   adc_rd_en_reg : std_logic := '0';
signal     adc_rd_busy : std_logic := '0';
signal adc_rd_valid_sr : std_logic_vector(16 downto 0) := (others => '0');


begin

--------------------------------------------------------------------------------
--data_proc:
--------------------------------------------------------------------------------
data_proc: process(mclk) begin
  if rising_edge(mclk) then
    --
    if (adc_rd_busy = '1') then
      adc_rd_en_reg <= '0';
    elsif (adc_rd_en = '1') then
      adc_rd_en_reg <= '1';
    end if;
    --
    if ((mrst or adc_rd_valid_sr(16)) = '1') then
      adc_rd_busy <= '0';
    elsif (((adc_rd_en or adc_rd_en_reg) and (clk_en(2) and (not sclk_int))) = '1') then
      adc_rd_busy <= '1';
    end if;
    --
    if (((clk_en(2) and (not sclk_int)) or adc_rd_valid_sr(16)) = '1') then
      adc_rd_valid_sr <= adc_rd_valid_sr(15 downto 0) & (adc_rd_en or adc_rd_en_reg);
    end if;
    --
    if (mrst = '1') then
      cs_n_int <= '1';
       sout_sr <= "0000" & adc_ch;
    elsif (((adc_rd_en or adc_rd_en_reg) and (clk_en(2) and (not sclk_int))) = '1') then
       sout_sr <= "0000" & adc_ch;
      cs_n_int <= '0';
    elsif(adc_rd_valid_sr(16) = '1') then
      cs_n_int <= '1';
    elsif ((clk_en(2) and sclk_int and adc_rd_busy) = '1') then
       sout_sr <= sout_sr(4 downto 0) & '0';
      cs_n_int <= '0';
    end if;
    --
    if ((adc_rd_busy and clk_en(2) and sclk_int) = '1') then
      sin_sr <= sin_sr(14 downto 0) & sin;
    end if;
    --
    cs_n_int_sr <= cs_n_int_sr(3 downto 0) & cs_n_int;
    cs_n <= cs_n_int_sr(4);
    sclk <= sclk_int;
    sout <= sout_sr(5);
    --
    if (adc_rd_valid_sr(16) = '1') then
      adc_data <= sin_sr(10 downto 3);
      adc_data_valid <= '1';
    else
      adc_data_valid <= '0';
    end if;
  end if;
end process data_proc;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- sclk_proc: generates 2.5 MHz serial clock 50/50 Duty Cycle
--------------------------------------------------------------------------------
sclk_proc: process (mclk) begin
  if rising_edge(mclk) then
    clk_en <= clk_en(1 downto 0) & clk_en(2);
    if (mrst = '1') then
      sclk_int <= '1';
    elsif (clk_en(2) = '1') then
      sclk_int <= not sclk_int;
    end if;
  end if;
end process sclk_proc;
--------------------------------------------------------------------------------

end architecture rtl;
--------------------------------------------------------------------------------
