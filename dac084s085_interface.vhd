

library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------
entity dac084s085_interface is
  port (-- Global Clock and Reset
         mclk : in std_logic;
         mrst : in std_logic;
        -- Interface signals to module interacting with DAC
            dac_data : in  std_logic_vector(7 downto 0);
              dac_ch : in  std_logic_vector(1 downto 0);
             dac_cmd : in  std_logic_vector(1 downto 0);
              dac_wr : in  std_logic;
         dac_wr_done : out std_logic;
        -- Interfacing signal to DAC084S085
           sclk : out std_logic;
         sync_n : out std_logic;
           sout : out std_logic
       );
end entity dac084s085_interface;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
architecture rtl of dac084s085_interface is

-- DAC MAX Clock Rate is 40MHz. Here it is set to 25MHz
signal     sync_n_int : std_logic := '1';
signal       sclk_int : std_logic := '1';
signal    dac_wr_busy : std_logic := '0';
signal     dac_wr_reg : std_logic := '0';
signal        sout_sr : std_logic_vector(11 downto 0) := (others => '0');
signal dac_wr_done_sr : std_logic_vector(31 downto 0) := (others => '0');
--------------------------------------------------------------------------------
--
-- Format of Data Input to DAC
--   __   __   __   __   __   __   __   __   __   __   __   __   __   __   __   __
--  |15| |14| |13| |12| |11| |10| |09| |08| |07| |06| |05| |04| |03| |02| |01| |00|
--
--  15 & 14 => selects DAC Channel. Mapped to dac_ch input
--  00 -> DAC A
--  01 -> DAC B
--  10 -> DAC C
--  11 -> DAC D
--
--  13 & 12 => selects DAC mode. Mapped to dac_cmd input
--  00 -> Write to specified register but do not update outputs
--  01 -> Write to specified register and update outputs
--  10 -> Write to all register and update outputs
--  11 -> power down outputs
--
--  11 to 4 => 8 Data Bits MSB First. Mapped to dac_data input
--
--  3 to 0 => Dont Care
--------------------------------------------------------------------------------

begin

--------------------------------------------------------------------------------
-- data_proc:
--------------------------------------------------------------------------------
data_proc: process(mclk) begin
  if rising_edge(mclk) then
    --
    if (mrst = '1') then
      sclk_int <= '1';
    else
      sclk_int <= not sclk_int;
    end if;
    --
    dac_wr_reg <= dac_wr;
    dac_wr_done_sr <= dac_wr_done_sr(30 downto 0) & (sclk_int and (dac_wr or dac_wr_reg));
    if ((mrst or dac_wr_done_sr(31)) = '1') then
      dac_wr_busy <= '0';
    elsif ((sclk_int and (dac_wr or dac_wr_reg)) = '1') then
      dac_wr_busy <= '1';
    end if;
    --
    if (mrst = '1') then
         sout_sr <= (others => '0');
      sync_n_int <= '1';
    elsif ((sclk_int and (dac_wr or dac_wr_reg)) = '1') then
      sout_sr <= dac_ch & dac_cmd & dac_data;
      sync_n_int <= '0';
    elsif(dac_wr_done_sr(31) = '1') then
      sync_n_int <= '1';
    elsif ((sclk_int and dac_wr_busy) = '1') then
         sout_sr <= sout_sr(10 downto 0) & '0';
      sync_n_int <= '0';
    end if;
  -- Registering outputs
      sclk <= not sclk_int;
      sout <= sout_sr(11);
    sync_n <= sync_n_int;
  end if;
end process data_proc;
dac_wr_done <= dac_wr_done_sr(31);
--------------------------------------------------------------------------------

end architecture rtl;
--------------------------------------------------------------------------------
