library ieee;
use ieee.std_logic_1164.all;

entity dac_adc_top is
  port ( mclk : in std_logic;
         rst : in std_logic;
      -- Interfacing signal to ADC084S085
         adc_sclk : out std_logic;
         adc_cs_n : out std_logic;
         adc_sout : out std_logic;
          adc_sin : in  std_logic;
        --
        leds : out std_logic_vector(7 downto 0)
        );
end entity dac_adc_top;

architecture str of dac_adc_top is

component dac084s085_interface
  port (-- Global Clock and Reset
         mclk : in std_logic;
         mrst : in std_logic;
        -- Interface signals to module interacting with DAC
            dac_data : in  std_logic_vector(7 downto 0);
              dac_ch : in  std_logic_vector(1 downto 0);
             dac_cmd : in  std_logic_vector(1 downto 0);
              dac_wr : in  std_logic;
         dac_wr_done : out std_logic;
        -- Interfacing signal to DAC084S085
           sclk : out std_logic;
         sync_n : out std_logic;
           sout : out std_logic
       );
end component;

component adc084s021_interface
  port (-- Global Clock and Reset
         mclk : in std_logic;
         mrst : in std_logic;
        -- Interface signals to module interacting with ADC
               adc_data : out std_logic_vector(7 downto 0);
         adc_data_valid : out std_logic;
                 adc_ch : in  std_logic_vector(1 downto 0);
              adc_rd_en : in  std_logic;
        -- Interfacing signal to ADC084S085
         sclk : out std_logic;
         cs_n : out std_logic;
         sout : out std_logic;
          sin : in  std_logic
       );
end component;

signal adc_data : std_logic_vector(7 downto 0) := (others => '0');
signal adc_data_valid : std_logic := '0';
signal adc_rd_en : std_logic;


begin


inst_adc084s021_interface : adc084s021_interface port map (
            mclk => mclk,
            mrst => rst,
        adc_data => adc_data,
  adc_data_valid => adc_data_valid,
          adc_ch => "00",
       adc_rd_en => adc_rd_en,
             sin => adc_sin,
            sclk => adc_sclk,
            cs_n => adc_cs_n,
            sout => adc_sout
  );

inst_dac084s085_interface : dac084s085_interface port map (
         mclk => mclk,
         mrst => rst,
     dac_data => adc_data,
       dac_ch => "01",
      dac_cmd => "01",
       dac_wr => adc_data_valid,
  dac_wr_done => open,
         sclk => dac_sclk,
       sync_n => dac_sync_n,
         sout => dac_sout
  );


adc_read_proc: process (mclk) begin
  if rising_edge(mclk) then
    if (rst = '1') then
      adc_rd_en <= '1';
    else
      adc_rd_en <= adc_data_valid;
    end if;
    --
    leds <= adc_data;
  end if;
end process adc_read_proc;

end architecture str;
