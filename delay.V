module time_delay(clk, reset, state);
input clk;
input reset;
output state;
wire clk;
wire reset;
reg  state = 1'b0;

reg [9:0]counter =0;
reg check_state =0;

always @(posedge clk)
begin
if (reset == 1'b1)
begin
 check_state =1;
 state =1'b1;
end
if ((check_state ==1) &&(counter <=900) )
 counter=counter+1;
else
begin
state = 1'b0;
 check_state=0;
 counter=0;
end
end
endmodule



